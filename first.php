<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>form</title>
  </head>
  <body>
  <form>
  <div class="container py-4">
    <label for="exampleInputEmail1">Email address</label>
    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
    <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
  </div>
  <div class="container">
    <label for="exampleInputPassword1">Password</label>
    <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
  </div>
  <div class="form-check container">
    <input type="checkbox" class="form-check-input" id="exampleCheck1">
    <label class="form-check-label" for="exampleCheck1">Check me out</label>
  </div>
  <button type="submit" class="btn btn-primary container">Submit</button>
</form>
<?php
// artimethic operator 
echo "<br>";
echo"this is value of a and b is ";
$a =2;
$b = 35;

echo $b-$a; 
echo "<br>";
echo"this is value of a and b is ";
$a =23;
$b = 35;

echo $b*$a; 
echo "<br>";
echo"this is value of a and b is ";
$a =23;
$b = 35;

echo $b/$a; 
echo "<br>";
echo"this is value of a and b is ";
$a =5;
$b = 35;

echo $b+$a; 
// assignment operator 
$newbar = $a;
echo "value of the newbar ";
echo $newbar;
echo "<br>";
$newbar = $a;
// $newbar += 1;
// $newbar -= 1;
$newbar *= 2;
echo "value of the newbar";
echo $newbar;
echo "<br>";

// comperison operator 
echo "the is find value 1===4";
echo var_dump(1==4);
echo "<br>";
echo "the is find value 1!=4";
echo var_dump(1!=4);
echo "<br>";
echo "the is find value 1>=4";
echo var_dump(1>=4);
echo "<br>"; 
echo $a++;
// echo $a--; 
// echo --$a;
echo "<br>";
echo $a; 
// echo ++$a;
// echo "<h2>the is tag </h2>"

// logical operator 
// and  (&&)
// or  (||)
// xor

// not (!)

// $myVar = (false and true);
$myVar = (false ||
 true);
// $myVar = (true) and (true);
echo var_dump($myVar);

?>
<!-- comperison operator  -->

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </body>
</html>